# Wordle Adrián Sánchez

## Explicació de les variables i què fa o quina funció té cadascuna
#### listaparaules
És un un array de paraules de 5 lletres en les quals no es repeteix cap lletra.
#### paraula
Tria una paraula de forma aleatòria entre les paraules de l'array 'listaparaules'
#### intents
Número d'intents dels que disposa l'usuari per a endevinar la paraula. Inicialment disposa de 6 intents
#### posicion
La definim com a 'Int'. La funció d'aquesta variable és comprobar cada posició de la paraula coincideix amb la que ha introduit l'usuari.
#### paraulaUsuari
És un string que més endevant li demanarà a l'usuari que introdueixi una paraula de 5 lletres.
#### contadorLetras
És un valor. El que fa és comptar les vegades que hi ha una lletra a la paraula.


## Codi
Per a començar tenim un do-while, que ens repetirà tot el codi de dins mentre tingui com a mínim 1 intent o la paraula que hagi introduit l'usuari no sigui la correcta.

Dins del do-while el primer que ens trobem és que li demanem la paraula a l'usuari i la convertim a string. En cas de que la paraula introduida no sigui de 5 lletres la torna a demanar amb un while.

Després tenim un for, que el que fa és comptar les vegades que es repeteix una lletra a la paraula i després depenent del valor que tingui la pintarà d'un color o d'un altre.

El següent for comproba si una lletra està a la paraula que ha introduit l'usuari. Si una lletra està a la posició correcta en la paraula que ha introduit l'usuari respecte a la paraula a endevinar la pintarà de verd, si una lletra està a la paraula que ha introduit l'usuari però no està a la posició correcta respecte a la paraula a endevinar la pintarà de groc i si una lletra que ha introduit l'usuari a la seva paraula no està a la paraula a endevinar la deixarà tal cual (el fons de la consola ja és gris, per això no la pinta de cap color) i això ja indicarà que aquesta lletra no està a la paraula a endevinar.

Després de cada vegada que l'usuari introdueix una paraula es resta 1 intent. Si la paraula introduida és igual a la que ha de endevinar sortirà del do-while, si no tornarà a poder introduir una altra paraula per probar de endevinarla sempre i quan disposi de com a mínim 1 intent.

Per finalitzar trobem un if-else if. Si l'usuari ha endevinat la paraula li mostrarà un missatge felicitant-lo per haver endevinat la paraula. Si o bé no ha aconseguit endevinar la paraula perquè s'ha quedat sense intents li mostrarà quina era la paraula a endevinar.
